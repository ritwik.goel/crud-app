package Verticles.controller;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

public enum ApiController {
    INSTANCE;
    //this is the api controller router.
    //instance of a router that we pass
    public Router router(Vertx vertx){
        Router router= Router.router(vertx);
        router.get("/allNames").handler(ApiFunctions.INSTANCE::getAllNames);
        router.get("/addUser").handler(ApiFunctions.INSTANCE::addUser);
        router.get("/findUserDetails/:name").handler(ApiFunctions.INSTANCE::findUserDetails);
        return router;
    }
}
