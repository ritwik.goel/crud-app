package Verticles.controller;

import Configs.Configs;
import Verticles.data.Employee;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Inet4Address;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
public enum ApiFunctions {
    INSTANCE;
    public void getAllNames(RoutingContext ctx) {
        Connection con;
        Configs configs = new Configs();
        String stringData="";
        try {
            con = DriverManager.getConnection(configs.getUrl(), configs.getName(), configs.getPass());
            Statement st = con.createStatement();
            String query = "SELECT * FROM Employees";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                stringData += rs.getString("id") + " " + rs.getString("Name")+"\n";
            }
            st.close();
            con.close();
        } catch (SQLException | RuntimeException e) {
            throw new RuntimeException(e);
        }
        ctx.response().end(stringData);
    }

    public void addUser(RoutingContext ctx) {
        Connection con;
        Configs configs = new Configs();
        try {
            con = DriverManager.getConnection(configs.getUrl(), configs.getName(), configs.getPass());
            Statement st = con.createStatement();
            String query = "INSERT into Employees VALUES(43,'Goel');";
            st.executeUpdate(query);
            st.close();
            con.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        ctx.response().putHeader("Content-Type", "text/plain").end("User added!");


    }
    public void findUserDetails(RoutingContext ctx) {
        Connection con;
        Configs configs = new Configs();
        try {
            con = DriverManager.getConnection(configs.getUrl(), configs.getName(), configs.getPass());
            Statement st = con.createStatement();
            String name = ctx.request().getParam("name");
            String firstLetter=name.substring(0,1).toUpperCase();
            String searchName=firstLetter+name.substring(1);
            String query = "SELECT * FROM Employees WHERE Name='"+searchName+"';";
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                int n= Integer.parseInt(rs.getString("id"));
                if(n!=0){
                    ctx.response().putHeader("Content-Type", "text/plain").end("User found in db!");

                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        ctx.response().putHeader("Content-Type", "text/plain").end("User not found in db!");

//
//        JSONParser jsonParser = new JSONParser();
//        Object obj = null;
//        try {
//            obj = jsonParser.parse(new FileReader("src/main/java/Verticles/data/EmpData.json"));
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        } catch (ParseException e) {
//            throw new RuntimeException(e);
//        }
//        String jsonString=obj.toString();
//        System.out.println(jsonString);
//        String name = ctx.request().getParam("name");
//
//        if(jsonString.toLowerCase().contains(name.toLowerCase())){
//            ctx.is("application/json");
//            ctx.response().end((name+" is present"));
//        }
//        else {
//            ctx.is("application/json");
//            ctx.response().end((name+" is not present"));
//        }
//        ctx.end();
    }


}

