package Verticles;

import Verticles.controller.ApiController;
import Verticles.data.Employee;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

public class Application extends AbstractVerticle {
    @Override
    public void start() throws Exception {
        //System.out.println("Running ");

        //Create server here and test normal api
        HttpServer server= vertx.createHttpServer();
        Router router= Router.router(vertx);


        router.route("/static/*").handler(StaticHandler.create("root"));

        router.mountSubRouter("/api", ApiController.INSTANCE.router(vertx));
        //then mount more api
        server.requestHandler((Handler<HttpServerRequest>) router).listen(8888);
    }
}
