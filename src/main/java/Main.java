import Configs.Configs;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

import java.sql.*;

public class Main {
    public static Configs configs= new Configs();
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Vertx vertx= Vertx.vertx();
        vertx.deployVerticle("Verticles.Application",new DeploymentOptions().setInstances(4));//test this with jmeter
    }
}