# crud-X

Basic crud app to understand the structure of vertX and RxJava

## Installation

Use the package manager [mvn](https://mvnrepository.com/) to install foobar.

```bash
mvn clean install
```

## Static files

Basic static serve on 
```bash
http://localhost:8888/static/index.html
```

## Issues

Json append outside the array causing GET statements to fail after the hard-coded POST req.
Remove the added contents to make use of POST multiple times.



## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)